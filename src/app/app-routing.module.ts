import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SendMessageFormComponent } from './send-message-form/send-message-form.component';


const routes: Routes = [
      {
        path: "QuickText",
        component: SendMessageFormComponent,
      },
      { path: "", redirectTo: "QuickText", pathMatch: "full" }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
