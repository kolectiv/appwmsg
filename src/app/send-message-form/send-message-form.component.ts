import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder} from '@angular/forms';
import { PwaServiceService } from '../services/pwa-service.service';
import { SwUpdate } from '@angular/service-worker';
import { Country } from '../models/contry.model';

@Component({
  selector: 'app-send-message-form',
  templateUrl: './send-message-form.component.html',
  styleUrls: ['./send-message-form.component.css'],
})
export class SendMessageFormComponent implements OnInit {
  public sendMessageForm: FormGroup;
  submitted = false;
  public countryCodeList: Country[] = [
    {
      name: 'Seleccione un país',
      code: '',
      mask: ''
    },
    {
      name: 'República Dominicana',
      code: '1',
      mask: '(000)-000-0000'
    },
    {
      name: 'Haití',
      code: '509',
      mask: '00 00 0000'
    },
    {
      name: 'Estados Unidos',
      code: '1',
      mask: '(000)-000-0000'
    },
    {
      name: 'Italia',
      code: '39',
      mask: '000 000 0000'
    }
  ];


  public url: string;


  constructor(
    private fb: FormBuilder,
    private swUpdate: SwUpdate,
    // tslint:disable-next-line: variable-name
    public _pwaService: PwaServiceService
  ) {

    }

  closeUpdateModal() {
    this.modalUpdate.style.display = 'none';
  }
  updateApp() {
    window.location.reload();
  }

  closeModal() {
    this.modalIos.style.display = 'none';
  }
  open(){
    this.modalUpdate.style.display = 'block'
  }

  ngOnInit() {
    this.selectedCountry = this.countryCodeList[0];
    //Check For Updates
    let modalInfo = document.getElementById('myModalUpdate');
    this.swUpdate.available.subscribe((event) => {
      modalInfo.style.display = 'block';
    });
    this.modalUpdate = modalInfo;
    this.safariDeploy();
    this.installOnSafari();

      if (this.detectIosStandalone()) {
      /* This code will be executed if app is running standalone */
      this.isSafari = false;
    }

    this.sendMessageForm = this.fb.group({  
      countryCode: ['', Validators.required],
      destinationNumber: [
        '',
        [Validators.required],
      ],
      optionalMessage: [''],
    });
  }

  detectIosStandalone(){
    return ('standalone' in window.navigator) && (window.navigator['standalone']);
  }


  

  isSafari;
  public modalIos;
  public modalUpdate ;



  safariDeploy() {
    var ua = navigator.userAgent.toLowerCase();
    if (ua.indexOf('safari') != -1)  {
      if (ua.indexOf('chrome') > -1 ) {
        this.isSafari = false;
      } else {
        this.isSafari = true;
      }
    }else{
      this.isSafari = false;
    }
  }

  openModal() {
    this.modalIos.style.display = 'block';
  }
  
  installOnSafari() {
  let modalInfo = document.getElementById('myModalIos');
  window.onclick = (event) => {
      if (event.target === modalInfo) {
        modalInfo.style.display = 'none';
      }
    };
  this.modalIos = modalInfo;
  }

 

  installPwa(): void {
    this._pwaService.promptEvent.prompt();
  }


  get f() {
    return this.sendMessageForm.controls;
  }


  numberCleaner(string) {
    //Solo numeros
    var out = '';
    var filtro = '1234567890'; //Caracteres validos

    //Recorrer el texto y verificar si el caracter se encuentra en la lista de validos
    for (var i = 0; i < string.length; i++)
      if (filtro.indexOf(string.charAt(i)) != -1)
        //Se añaden a la salida los caracteres validos
        out += string.charAt(i);

    //Retornar valor filtrado
    this.sendMessageForm.value.destinationNumber = out;
  }

  validatedFormNumber = true;
  validatedFormCountry = true;

  
  onSubmit() {
    this.submitted = true;
    
    if (this.f['countryCode'].errors && this.f['destinationNumber'].errors) {
      this.validatedFormCountry = false;
      this.validatedFormNumber = false;
      return;
    } else if (this.f['countryCode'].errors) {
      this.validatedFormCountry = false;
      return;
    } else if (this.f['destinationNumber'].errors) {
      this.validatedFormNumber = false;
      return;
    }else {

    this.validatedFormCountry = true;
    this.validatedFormNumber = true;
    this.url =
      'https://wa.me/' +
    this.f['countryCode'].value +
    this.f['destinationNumber'].value +
      '?text=' +
    this.f['optionalMessage'].value;

    if (window.matchMedia('(display-mode: standalone)').matches) {
      window.open(this.url, '_self');
    } else {
      window.open(this.url, '_blank');
    }

    this.f['destinationNumber'].setValue('');
    this.f['optionalMessage'].setValue('');
    
    }


  }

  selectedCountry: Country;
  cellphoneMask:string;
  selectCountry() {
    this.validatedFormCountry = true;
    this.f['countryCode'].setValue(this.selectedCountry.code);
    this.cellphoneMask = this.selectedCountry.mask;
    console.log(this.cellphoneMask);
  }


  
}
